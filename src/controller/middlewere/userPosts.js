const express = require('express');
const userPostsRouter = express.Router(); // eslint-disable-line new-cap
const {registration} = require(`../../utils/utilities`);
const {notes} = require('../../model/DBcreation');

const noteCheck = async (req, res) => {
  const noteId = req.params.id;
  if (!noteId) {
    return res.status(400).json({message: 'wrong note id or note'});
  }
  const user = await registration(req, res);
  if (user) {
    try {
      const note = await notes.findOne({_id: noteId});
      if (note.userId != user._id) {
        throw new Error('you do not have the permission');
      }
      return note;
    } catch (err) {
      throw err;
    }
  }
};

userPostsRouter.post(`/notes`, async (req, res) => {
  const user = await registration(req, res);
  if (user && req.body.text) {
    const userId = user._id;
    const newPost = new notes({ // eslint-disable-line new-cap
      userId,
      completed: false,
      text: req.body.text,
      createdDate: new Date(Date.now()),
    });
    try {
      await newPost.save();
    } catch (err) {
      try {
        res.status(400).json({message: err.message});
      } catch (err) {
        console.log(`fix that shit`);
      }
    }
    return res.status(200).json({message: 'Success'});
  }
  res.status(400).json(
      {message: 'something wrong with registration or text field'},
  );
});

userPostsRouter.get(`/notes`, async (req, res, next) => {
  let offset;
  let limit;
  if (req.params.id) {
    next();
  }
  if (!req.query.offset) {
    offset = 0;
  } else {
    offset = Number.parseInt(req.query.offset);
  }

  if (!req.query.offset) {
    limit = 10;
  } else {
    offset = Number.parseInt(req.query.limit);
  }

  const user = await registration(req, res);
  if (user) {
    const options = {
      page: 1,
      limit,
      offset,
    };
    notes.paginate({userId: user._id}, options, (err, result) => {
      if (err) {
        return res.status(500).json({message: 'something went wrong'});
      }
      
      return res.status(200).json({
        offset,
        limit,
        count: result.totalPages,
        notes: result.docs,
      });
    });
  }
});

userPostsRouter.route(`/notes/:id`)
    .get(async (req, res) => {
      try {
        const note = await noteCheck(req, res);
        res.status(200).json({note});
      } catch (err) {
        try {
          res.status(400).json({message: err.message});
        } catch (err) {
          console.log(`fix that shit`);
        }
      }
    })
    .put(async (req, res) => {
      try {
        const note = await noteCheck(req, res);
        if (!req.body.text) {
          throw new Error(`text field empty/or named inccorectly`);
        }
        await notes.findOneAndUpdate({_id: note._id}, {text: req.body.text});
        res.status(200).json({message: 'success'});
      } catch (err) {
        try {
          res.status(400).json({message: err.message});
        } catch (err) {
          console.log(`fix that shit`);
        }
      }
    })
    .patch(async (req, res) => {
      try {
        const note = await noteCheck(req, res);
        if (note.completed == false) {
          await notes.findOneAndUpdate({_id: note._id}, {completed: true});
        } else {
          await notes.findOneAndUpdate(
              {_id: note._id}, {completed: false},
          );
        }
        res.status(200).json(
            {message: `complited changed to ${!note.completed}`},
        );
      } catch (err) {
        try {
          res.status(400).json({message: err.message});
        } catch (err) {
          console.log(`fix that shit`);
        }
      }
    })
    .delete(async (req, res) => {
      try {
        const note = await noteCheck(req, res);
        await notes.deleteOne({_id: note._id});
        res.status(200).json({message: `post was deleted`});
      } catch (err) {
        try {
          res.status(400).json({message: err.message});
        } catch (err) {
          console.log(`fix that shit`);
        }
      }
    });
module.exports = userPostsRouter;
