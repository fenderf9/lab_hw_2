const express = require('express');
const accountRouter = express.Router(); // eslint-disable-line new-cap
const {
  registration,
  comparePassword,
  hashPassword,
} = require(`../../utils/utilities`);
const {users} = require('../../model/DBcreation');

accountRouter.route(`/me`)
    .get(async (req, res) => {
      const user = await registration(req, res);
      if (user) {
        res.status(200).json({
          user: {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate,
          },
        });
      }
    })
    .delete(async (req, res) => {
      const user = await registration(req, res);
      if (user) {
        await user.deleteOne({username: user.username});
        res.status(200).json({message: 'success!'});
      }
    })
    .patch(async (req, res) => {
      const user = await registration(req, res);
      const {oldPassword, newPassword} = req.body;

      if (!oldPassword || !newPassword) {
        return res.status(400).send(
            {'message': 'old or new fields with password didnt fill'},
        );
      }

      if (! await comparePassword(oldPassword, user.password)) {
        return res.status(400).json({message: 'inccorect password'});
      }
      const hash = await hashPassword(newPassword, res);
      try {
        await users.findOneAndUpdate(
            {username: user.username},
            {password: hash},
        );
        await user.save();
        res.status(200).json({message: 'password change'});
        console.log(await comparePassword(oldPassword, user.password));
      } catch (err) {
        return res.status(400).json({message: err.message});
      }
    });

module.exports = accountRouter;
