const express = require('express');
const authRouter = express.Router();// eslint-disable-line new-cap
const jwt = require(`jsonwebtoken`);
require(`dotenv`).config();
const {users} = require('../../model/DBcreation');
const {comparePassword, hashPassword} = require(`../../utils/utilities`);

authRouter.post(`/register`, async (req, res) => {
  const password = await hashPassword(req.body.password, res);

  const newUser = new users({ // eslint-disable-line new-cap
    password,
    username: req.body.username,
    createdDate: new Date(Date.now()),
  });
  try {
    await newUser.save();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
  res.status(200).json({message: 'Success'});
});
authRouter.post(`/login`, async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const user = await users.findOne({username});

  if (!user) {
    return res.status(400).json({message: 'inccorect email/password'});
  }

  if (! await comparePassword(password, user.password)) {
    return res.status(400).json({message: 'inccorect email/password'});
  }

  const token = jwt.sign({username}, process.env.TOKEN_SECRET);
  res.status(200).json({
    'message': 'Success',
    'jwt_token': token,
  });
});


module.exports = authRouter;
