const express = require('express');
const app = express();
const morgan = require('morgan');
const authRouter = require('./middlewere/auth');
const accountRouter = require('./middlewere/account');
const userPostsRouter = require('./middlewere/userPosts');
const port = 8080;

app.use(express.json());
app.use(morgan(`tiny`));
app.use(`/api/auth`, authRouter);
app.use(`/api/users`, accountRouter);
app.use(`/api`, userPostsRouter);
app.use(`/*`, (req,res) => {
  res.status(200).json({messge: "unfortynatly, this request did not support"})
});


app.listen(port, (err) =>{
  if (!err) {
    console.log(`Server is running on port ${port}`);
    return;
  }
  console.error(err);
});
