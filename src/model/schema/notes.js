const mongoose = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
    required: true,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: String,
  },
});
noteSchema.plugin(paginate);
module.exports = noteSchema;
