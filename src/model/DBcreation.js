const mongoose = require('mongoose');
const userSchema = require('./schema/users.js');
const noteSchema = require('./schema/notes.js');


mongoose.connect(process.env.DB_PATH, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});


const users = mongoose.model(`user`, userSchema);
const notes = mongoose.model(`note`, noteSchema);

module.exports = {users, notes};
