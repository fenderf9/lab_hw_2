const bcrypt = require('bcrypt');
const jwt = require(`jsonwebtoken`);
const {users} = require('../model/DBcreation');

const comparePassword = async (password, user) =>{
  if (await bcrypt.compare(password, user)) {
    return true;
  }
  return false;
};

const hashPassword = async (pas, res) => {
  const salt = 10;
  try {
    if (!pas) {
      const err = new Error(`there is no password!`);
      throw err;
    }
    const password = await bcrypt.hash(pas, salt);
    return password;
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const registration = async (req, res) => {
  try {
    const decoded = jwt.verify(
        req.headers.authorization.split(' ')[1], process.env.TOKEN_SECRET,
    );
    const user = await users.findOne({username: decoded.username});
    if (!user) {
      res.status(400).json({message: 'this user don`t exist any more'});
      return false;
    }
    return user;
  } catch (err) {
    res.status(400).json({message: err.message});
    return false;
  }
};

module.exports = {
  comparePassword,
  registration,
  hashPassword,
};
